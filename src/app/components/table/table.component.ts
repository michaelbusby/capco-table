import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnChanges {
  @Output() submitEmitter: EventEmitter<number> = new EventEmitter<number>();

  columns: string[];
  pageNumbers: number[];
  currentPage = 0;

  @Input() numRows = 25;
  @Input() data: any[];

  // only get the data for the current page
  get currentPageData() {
    // starting index
    const start = this.currentPage * this.numRows;
    if (this.data && this.data.length > 0) {
      return this.data.slice(start, start + this.numRows);
    }
  }

  constructor() {}

  ngOnInit() {}

  /**
   *
   * @param changes object holding Input change information
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes.data) {
      const data = changes.data.currentValue;
      // if we have new data
      if (data && data.length > 0) {
        this.columns = Object.keys(data[0]);
        // add a button for each page
        this.pageNumbers = Array(Math.ceil(data.length / this.numRows))
          .fill(0)
          .map((x, i) => i);
      }
    }
  }

  /**
   * Change the page number
   * @param page page number
   */
  changePage(page: any) {
    this.currentPage = page;
  }

  /**
   * Emit the selected entry for the containing
   * component to handle the HTTP request
   * @param entryId id property of the entry
   */
  submit(entry: any) {
    this.submitEmitter.emit(entry);
  }
}
