import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'capco-table';

  tableData: any[];
  numRows = 25;

  constructor(private http: HttpClient) {
    this.http.get('../assets/sample_data.json')
    .pipe(first())
    .subscribe((data: any) => {
      this.tableData = data;
    });
  }

  submitId(id) {
    this.http.post('/api/submit', id).subscribe(entry => {});
  }
}
